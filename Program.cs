﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using MachineLearning;
using MachineLearning.BayesianClassifier;
using static MachineLearning.Data;

namespace BayesianClassifier
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = Stopwatch.StartNew();

            Data data;
            bool loadBinFileIfPossible = true;

            if(File.Exists("20_newsgroups.bin") && loadBinFileIfPossible)
            {
                Console.WriteLine("Found binary, deserializing..");
                data = DataSerialization.Deserialize<Data>("20_newsgroups.bin");
            }
            else{
                Console.WriteLine("Could not find binary, reading directory structure..");
                data = ReadData("20_newsgroups");
            }

            if(!File.Exists("20_newsgroups.bin")){
                DataSerialization.Serialize("20_newsgroups.bin",data);
            }
            
            var training = Data.GetDataSubset(data, TakeFrom.Beginning, 0.8f);
            var testing = Data.GetDataSubset(data, TakeFrom.End, 0.2f);
            
            Classifier classifier;
            if(File.Exists("classifier.bin")){
                classifier = DataSerialization.Deserialize<Classifier>("classifier.bin");
            }
            else {
                classifier = new Classifier();
                Console.WriteLine("Training..");
                classifier.Train(training,true);
                Console.WriteLine("Training complete");
                DataSerialization.Serialize<Classifier>("classifier.bin",classifier);
            }

            var correct = 0;
            var total = 0;
            var categoryAccuracy = new Dictionary<string, int>();
            foreach(var c in testing.FilesCategorized.Keys){
                foreach(var f in testing.FilesCategorized[c]){
                    var result = classifier.Classify(f, true).Where(k => k.Value > 0.0001).OrderByDescending(k => k.Value);
                    total ++;
                    if(result.Count() > 0 && result.First().Key == c){
                        categoryAccuracy[c] = categoryAccuracy.ContainsKey(c) ? categoryAccuracy[c] +1 : 1;
                        correct++;
                    }
                    if(total % 100 == 0){
                        Console.Clear();
                        Console.WriteLine($"Accuracy: {((float)correct/total)*100.0}%");
                    }
                }
               
            }

            foreach(var c in categoryAccuracy){
                Console.WriteLine($"{c.Key}: {c.Value/200.0*100.0}%");
            }

            sw.Stop();
            Console.WriteLine($"Timer: {sw.ElapsedMilliseconds/1000.0}s");
        }
        private static Data ReadData(string v)
        {
            Data data = new Data();
            data.Directories = Directory.EnumerateDirectories("20_newsgroups","*",SearchOption.AllDirectories).ToList();

            foreach(var directory in data.Directories){
                List<string> files = new List<string>();
                foreach(var file in Directory.EnumerateFiles(directory)){
                    files.Add( File.ReadAllText(file));
                }
                data.Categories.Add(directory.Split('\\').Last());
                data.FilesCategorized.Add(directory.Split('\\').Last(),files);
            }

            return data;
        }

        

    }
}
