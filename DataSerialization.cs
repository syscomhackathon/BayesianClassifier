using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class DataSerialization{
    public static void Serialize<T>(string filename, T data)
    {
            FileStream fs = new FileStream(filename, FileMode.Create);

            // Construct a BinaryFormatter and use it to serialize the data to the stream.
            BinaryFormatter formatter = new BinaryFormatter();
            try 
            {
                formatter.Serialize(fs, data);
            }
            catch (SerializationException e) 
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally 
            {
                fs.Close();
            }
        }

        public static T Deserialize<T>(string filename) 
        {
            // Declare the hashtable reference.
            T data  = default(T);

            // Open the file containing the data that you want to deserialize.
            FileStream fs = new FileStream(filename, FileMode.Open);
            try 
            {
                BinaryFormatter formatter = new BinaryFormatter();

                // Deserialize the hashtable from the file and 
                // assign the reference to the local variable.
                data = (T) formatter.Deserialize(fs);

                return data;
            }
            catch (SerializationException e) 
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally 
            {
                fs.Close();
            }
        }
}