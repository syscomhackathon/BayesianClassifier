using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Annytab.Stemmer;

namespace MachineLearning.BayesianClassifier
{
    [Serializable]
    public class Classifier {
        public string[] StopWords { get; set; }
        private readonly Dictionary<string,Dictionary<string,double>> wordFrequencies;
        private readonly Dictionary<string,Dictionary<string,double>> wordFrequenciesIverseDocumentFrequency;
        private readonly List<string> categories;
        private Dictionary<string,int> categoryDocCount;
        private Dictionary<string,int> documentFrequency;
        private double DocumentCount;

        public Classifier(string[] stopWords = null){
            if(stopWords != null){
                StopWords = stopWords;
            }
            else{
                StopWords = MachineLearning.StopWords.English;
            }
            wordFrequencies = new Dictionary<string,Dictionary<string,double>>();
            wordFrequenciesIverseDocumentFrequency = new Dictionary<string,Dictionary<string,double>>();
            categoryDocCount = new Dictionary<string,int>();
            categories = new List<string>();
        }

        public void Train(Data data, bool transform)
        {
            DocumentCount = data.FilesCategorized.Select(i => data.FilesCategorized[i.Key].Count()).Sum();
            categoryDocCount = data.FilesCategorized.ToDictionary(k => k.Key, v => data.FilesCategorized[v.Key].Count());
            categories.AddRange(data.Categories);
            var concurrentDocumentFrequencies = new ConcurrentDictionary<string,int>();
            Parallel.ForEach(categories, category => 
            {
                //P(category)
                double pCat = categoryDocCount[category]/DocumentCount;

                var bagOfWords = new List<string>();
                foreach(var file in data.FilesCategorized[category])
                {
                    var tokens = Tokenize(file);
                    if(transform)
                        tokens.Distinct().Select(t => {
                            concurrentDocumentFrequencies.AddOrUpdate(t,1,(k,v) => v+1);
                            return 0;
                        }).ToList();
                    bagOfWords.AddRange(tokens);
                }
                
                wordFrequencies[category] = new Dictionary<string,double>();

                foreach(var word in bagOfWords){
                    double wordCount = bagOfWords.Count(w => w == word);

                    //P(word|category)
                    wordFrequencies[category][word] = wordCount/categoryDocCount[category];
                }
            });
            documentFrequency = concurrentDocumentFrequencies.ToDictionary(e => e.Key, e => e.Value);
            
            if(transform){
                TfIdf();
            }
        }

        public Dictionary<string, double> Classify(string fileContent, bool transform)
        {
            var result = new List<string>();
            var classifications = new Dictionary<string, double>();
            var tokens = Tokenize(fileContent);

            foreach(var category in categories)
            {
                classifications[category] = categoryDocCount[category]/DocumentCount;
                if(!transform)
                    foreach(var word in tokens)
                    {
                        if(wordFrequencies[category].ContainsKey(word))
                            classifications[category] = classifications[category] * wordFrequencies[category][word];
                        else
                            classifications[category] = classifications[category] * 1.0D/categoryDocCount[category];
                    }
                else {
                    foreach(var word in tokens)
                    {
                        if(wordFrequenciesIverseDocumentFrequency[category].ContainsKey(word))
                            classifications[category] = classifications[category] * wordFrequenciesIverseDocumentFrequency[category][word];
                        else
                            classifications[category] = classifications[category] * TfIdfCalculationClassification(category,word);
                    }
                }
            }
            if(classifications.Any(k => double.IsNaN(k.Value))|| classifications.All(k => k.Value == 0))
                Debugger.Break();

            return classifications.ToDictionary(k => k.Key, k => k.Value/classifications.Select(v => v.Value).Sum());
        }

        private void TfIdf(){
            wordFrequencies.Keys.Select(wf => {
                wordFrequenciesIverseDocumentFrequency
                    .Add(wf, TfIdfCalculation(wf));
                    return 0;
                    }).ToList();
        }

        private Dictionary<string, double> TfIdfCalculation(string category){
            return wordFrequencies[category]
                .ToDictionary(k => k.Key, k => TfIdfCalculation(category, k.Key));
        }

        private double TfIdfCalculation(string category, string term){
            double wf =  wordFrequencies[category].ContainsKey(term) ? wordFrequencies[category][term] : 1;
            double df =  documentFrequency.ContainsKey(term) ? documentFrequency[term] : 1;
                
            return wf * Math.Log(DocumentCount/df)+1;
        }

        private double TfIdfCalculationClassification(string category, string term){
            double tf = 1.0D/categoryDocCount[category];
            if(wordFrequencies[category].ContainsKey(term))
                tf = wordFrequencies[category][term];
            

            double df = DocumentCount+1/1.0D;
            if(documentFrequency.ContainsKey(term))
                df = DocumentCount/documentFrequency[term];

            return tf * Math.Log(df)+1;
        }

        private List<string> Tokenize(string text){
            IStemmer stemmer = new EnglishStemmer();
            var bagOfWords = text.ToLower()
                .Split(new char[] {' ','\n','!','.',',','?',':','-','\"','(',')','<','>','_','*','\'','[',']','{','}',';','#','|','\\','/','\t','='})
                .Where(w => w != string.Empty && !w.Contains("@"));

            return stemmer.GetSteamWords(bagOfWords.Except(StopWords).ToArray()).ToList();
        }
    }
}