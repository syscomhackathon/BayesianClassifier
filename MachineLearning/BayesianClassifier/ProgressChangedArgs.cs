using System;
using System.Collections.Generic;

namespace MachineLearning.BayesianClassifier
{
    public class ProgressChangedArgs : EventArgs
    {
        public  Dictionary<string, int> Progress;
    }
}