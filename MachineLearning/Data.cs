using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace MachineLearning
{
    [Serializable]
    public class Data : ICloneable {
        public Data(){
            Directories = new List<string>();
            Categories = new List<string>();
            FilesCategorized = new Dictionary<string, List<string>>();
        }
        public List<string> Directories {get; set;}
        public List<string> Categories {get; set;}
        public Dictionary<string, List<string>> FilesCategorized {get; set;}

        public enum TakeFrom  {
            Beginning,
            End
        }
        public static Data GetDataSubset(Data data, TakeFrom takeFrom, float portion = 0.5f){
            if(portion > 1.0f){
                portion = 1.0f;
            }

            Dictionary<string, List<string>> dataSet = new Dictionary<string, List<string>>();

            foreach(var fileList in data.FilesCategorized){
                if(takeFrom == TakeFrom.Beginning)
                    dataSet.Add(fileList.Key,fileList.Value.Take((int)(fileList.Value.Count()*portion)).ToList());

                if(takeFrom == TakeFrom.End)
                    dataSet.Add(fileList.Key,fileList.Value.Skip((int)(fileList.Value.Count()*(1.0f-portion))).ToList());
            }

            Data result = (Data)data.Clone();
            result.FilesCategorized = dataSet;
            return result;
        }
        public object Clone()
        {
            return new Data {
                Categories = ((string[])this.Categories.ToArray().Clone()).ToList(),
                Directories = ((string[])this.Directories.ToArray().Clone()).ToList(),
                FilesCategorized = this.Categories.ToDictionary(k => k, v => ((string[])this.FilesCategorized[v].ToArray().Clone()).ToList())
            };
        }
    }
}
